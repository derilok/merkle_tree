#Merkle tree implementation in Rust

First of all merkle tree is a binary tree structure. End nodes (aka Leafs) can have a value and hash. Every Leaf parent's hash makes combining hashes of its Leaf hashes and hashes it. I used SHA256 algorithm for hashing.
To build a Merkle Tree we should pass a vector of vectors which represents binary data.
We uses this vector as a stack to get values from the beginnigng, hashes it and put back a binary tree.
We can get a hash of current tree using function `hash()` disregard of we have a Leaf or a Node.
When only one item left on the stack we consider it a Merkle Tree's root
Merkle Tree also has `root_hash()` function which represents a hash of top level node

Cons:
- We cannot insert/remove any data to merkle tree
- We cannot audit the merkle tree to be sure that all data is unchanged.