use utils::hash_bytes;

pub enum Tree<T> {
    Leaf {
        hash: Vec<u8>,
        data: T,
    },
    Node {
        hash: Vec<u8>,
        left: Box<Tree<T>>,
        right: Box<Tree<T>>,
    }
}

impl <T> Tree<T> where T: Into<Vec<u8>> + Clone {
    pub fn new(hash: Vec<u8>, value: T) -> Self {
        Tree::Leaf {
            hash: hash,
            data: value,
        }
    }

    pub fn make_leaf(value: T) -> Self {
        let hash = hash_bytes(&value.clone().into());
        Tree::new(hash, value)
    }

    pub fn hash(&self) -> &Vec<u8> {
        match *self {
            Tree::Leaf { ref hash, .. } | Tree::Node { ref hash, .. } => hash
        }
    }
}