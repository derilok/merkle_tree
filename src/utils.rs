use ring::digest;

pub fn hash_bytes(bytes: &[u8]) -> Vec<u8> {
    let algo = &digest::SHA256;
    let mut context = digest::Context::new(&algo);
    context.update(bytes);
    context.finish().as_ref().into()
}

pub fn combine_hashes(left: &[u8], right: &[u8]) -> Vec<u8> {
    let algo = &digest::SHA256;
    let mut context = digest::Context::new(&algo);
    let combined = [left, right].concat();
    context.update(&combined);
    context.finish().as_ref().into()
}