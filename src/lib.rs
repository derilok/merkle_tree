extern crate ring;

pub mod merkle_tree;
pub mod tree;
mod utils;

#[cfg(test)]
mod tests {
    use utils::{hash_bytes, combine_hashes};
    use merkle_tree::MerkleTree;

    #[test]
    fn from_vector() {
        let values = (1..4).map(|x| vec![x]).collect::<Vec<_>>();
        let hashes = vec![
            hash_bytes(&values[0]),
            hash_bytes(&values[1]),
            hash_bytes(&values[2]),
        ];
        let count = values.len();
        let tree = MerkleTree::new(values).unwrap();
        let hash1 = combine_hashes(&hashes[0], &hashes[1]);
        let hash2 = &hashes[2];
        let root_hash = combine_hashes(&hash1, &hash2);

        assert_eq!(tree.count(), count);
        assert_eq!(tree.height(), 2);
        assert_eq!(tree.root_hash(), &root_hash);
    }
    
    #[test]
    #[should_panic]
    fn test_from_empty() {
        let values: Vec<Vec<u8>> = vec![];
        MerkleTree::new(values).unwrap();
    }
}
