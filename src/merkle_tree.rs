use tree::Tree;
use utils::combine_hashes;

pub struct MerkleTree<T> {
    root: Tree<T>,
    height: usize,
    count: usize,
}

impl <T> MerkleTree<T>
    where T: Into<Vec<u8>> + Clone
{
    pub fn new(values: Vec<T>) -> Option<Self> {
        if values.is_empty() {
            return None
        }

        let count = values.len();
        let mut height = 0;
        let mut cur = vec![];

        for value in values {
            let leaf = Tree::make_leaf(value);
            cur.push(leaf);
        }

        while cur.len() > 1 {
            let mut next = Vec::new();
            while !cur.is_empty() {
                if cur.len() == 1 {
                    next.push(cur.remove(0));
                }
                else {
                    let left = cur.remove(0);
                    let right = cur.remove(0);

                    let combined_hash = combine_hashes(left.hash(), right.hash());
                    let node = Tree::Node {
                        hash: combined_hash,
                        left: Box::new(left),
                        right: Box::new(right),
                    };
                    next.push(node);
                }
            }

            height += 1;
            cur = next;
        }

        let root = cur.remove(0);
        
        Some(MerkleTree {
            root,
            height,
            count,
        })
    }

    pub fn count(&self) -> usize {
        self.count
    }

    pub fn height(&self) -> usize {
        self.height
    }

    pub fn root_hash(&self) -> &Vec<u8> {
        self.root.hash()
    }
}